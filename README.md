## Weather forecast 

For this project, I used MVVM pattern. I added some helpers and tests. Written in Xcode 12. Since 3rd party libraries not allowed, I didn't use Swift Lint.

There were couple of options to build local data / live data architecture. 

I could make weather list as a component. When segmented controller changed, services for live and local data could be injected just like strategy pattern. But I decided to use 2 different function.

If there is something not clear, please let me know. I would love to discuss on it. 
