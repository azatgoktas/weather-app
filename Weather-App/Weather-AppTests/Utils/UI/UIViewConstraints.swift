//
//  UIViewConstraints.swift
//  Weather-AppTests
//
//  Created by Azat Goktas on 26.01.2021.
//

import XCTest
@testable import Weather_App

final class UIVIewConstraints: XCTestCase {

    func testAddSubview() {
        let view = UIView()
        let viewToTest = UIView()
        
        view.WA_addSubview(viewToTest)
        
        XCTAssertEqual(view, viewToTest.superview)
    }
    
    func testAddSubviewWithInset() {
        let view = UIView()
        let viewToTest = UIView()
        let edge = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        view.WA_addSubview(viewToTest, insets: edge)
        
        
        XCTAssertEqual(view.constraints.count, 4)
    }
    
    func testAddSubviewWithInsetAndExcludedEdge() {
        let view = UIView()
        let viewToTest = UIView()
        let edge = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        view.WA_addSubview(viewToTest, insets: edge, excludingEdge: .bottom)
        
        
        XCTAssertEqual(view.constraints.count, 3)
    }
}
