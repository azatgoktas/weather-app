//
//  DateString.swift
//  Weather-AppTests
//
//  Created by Azat Goktas on 26.01.2021.
//

import XCTest
@testable import Weather_App

final class DateString: XCTestCase {
    func testDay() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-mm-YYYY"
        dateFormatter.locale = Locale(identifier: "EN-en")
        guard let date = dateFormatter.date(from: "26-01-2021") else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(date.day, "Monday")
    }
}
