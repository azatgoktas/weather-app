//
//  OptionalEmpty.swift
//  Weather-AppTests
//
//  Created by Azat Goktas on 26.01.2021.
//

import XCTest
@testable import Weather_App

final class OptionalEmpty: XCTestCase {
    func testOrEmpty() {
        var string: String?
        
        XCTAssertEqual(string.orEmpty, "")
        
        string = "someText"
        
        XCTAssertEqual(string, "someText")
    }
}
