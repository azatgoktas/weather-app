//
//  Weather.swift
//  Weather-AppTests
//
//  Created by Azat Goktas on 26.01.2021.
//

import XCTest
@testable import Weather_App

final class Weather: XCTestCase {
    
    private var view: MockView!
    private var viewModel: WeatherViewModel!
    
    override func setUp() {
        let mockService = MockWeatherService()
        view = MockView()
        viewModel = WeatherViewModel(service: mockService)
        viewModel.delegate = view
    }
    
    func testLoad() {
        // given
        // get data from local
        viewModel.changeData(index: 1)
        
        // when
        viewModel.load()
        
        // then
        
        XCTAssertEqual(view.outputs.count, 5)
        
        if let firstOutput = view.outputs.first {
            switch firstOutput {
            case .setEmptyViewHidden:
                break
            default:
                XCTFail("first output is not update title")
            }
        }
        
        XCTAssertEqual(view.outputs[2], .updateTitle("Weather Forecast"))
    }
    
}

