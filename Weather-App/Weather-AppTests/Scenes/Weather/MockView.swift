//
//  MockView.swift
//  Weather-AppTests
//
//  Created by Azat Goktas on 26.01.2021.
//

import Foundation
@testable import Weather_App

final class MockView: WeatherViewModelDelegate {
    
    var outputs: [WeatherViewModelOutput] = []

    func handleViewModelOutput(_ output: WeatherViewModelOutput) {
        outputs.append(output)
    }
}
