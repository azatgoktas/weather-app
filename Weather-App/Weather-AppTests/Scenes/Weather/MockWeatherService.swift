//
//  MockWeatherService.swift
//  Weather-AppTests
//
//  Created by Azat Goktas on 26.01.2021.
//

import Foundation
@testable import Weather_App


final class MockWeatherService: WeatherServiceProtocol {
    var weatherResponse: WeatherResponse!
    
    func getWeather(query: String, completion: @escaping (Result<WeatherResponse, Error>) -> Void) {
        completion(.success(weatherResponse))
    }
    
}
