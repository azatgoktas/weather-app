//
//  EmptyValueRepresentable.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation

protocol EmptyValueRepresentable {

    static var emptyValue: Self { get }
}

extension String: EmptyValueRepresentable {

    static var emptyValue: String { return "" }
}
