//
//  EmptyPresentation.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

struct EmptyPresentation {
    let message: String
    let imageName: String
}

extension EmptyPresentation: Equatable {}
