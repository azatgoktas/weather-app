//
//  EmptyView.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

final class EmptyView: NibView {
    
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    
    func fill(_ presentation: EmptyPresentation) {
        messageLabel.text = presentation.message
        imageView.image = UIImage(named: presentation.imageName)
    }
}

