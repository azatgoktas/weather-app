//
//  NibView.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

class NibView: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        ownFirstNib()
    }

    required override init(frame: CGRect) {
        super.init(frame: frame)

        ownFirstNib()
    }
}

