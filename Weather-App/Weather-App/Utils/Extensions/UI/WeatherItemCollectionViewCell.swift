//
//  WeatherItemCollectionViewCell.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

final class WeatherItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private(set) weak var tempratureLabel: UILabel!
    @IBOutlet private(set) weak var descriptionLabel: UILabel!
    @IBOutlet private(set) weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        WA_applyStyling()
    }

}

// MARK: - Public

extension WeatherItemCollectionViewCell {
    func fill(_ presentation: WeatherListItemPresentation) {
        tempratureLabel.text = presentation.temprature
        descriptionLabel.text = presentation.description
        timeLabel.text = presentation.time
        
        if let url = URL(string: presentation.imageUrl) {
            iconImageView.load(url: url)
        }
    }
}
