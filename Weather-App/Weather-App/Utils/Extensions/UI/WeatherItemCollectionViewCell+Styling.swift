//
//  WeatherItemCollectionViewCell+Styling.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

extension WeatherItemCollectionViewCell {
    
    private enum Constant {
        static let tempratureLabelFontSize: CGFloat = 15.0
        static let descriptionLabelFontSize: CGFloat = 12.0
        static let timeLabelFontSize: CGFloat = 9.0
    }
    
    func WA_applyStyling() {
        tempratureLabel.font = .systemFont(ofSize: Constant.tempratureLabelFontSize, weight: .medium)
        descriptionLabel.font = .systemFont(ofSize: Constant.descriptionLabelFontSize, weight: .regular)
        timeLabel.font = .systemFont(ofSize: Constant.timeLabelFontSize, weight: .regular)
    }
}
