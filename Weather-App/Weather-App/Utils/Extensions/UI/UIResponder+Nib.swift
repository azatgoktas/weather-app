//
//  UIResponder+Nib.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

extension UIResponder {

    class var defaultNib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
}
