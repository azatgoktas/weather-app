//
//  UIViewController+Alert.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

extension UIViewController {
    
    func WA_showAlert(title: String?,
                      message: String?,
                      placeholder: String,
                      handler: @escaping (String) -> Void) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addTextField { (textfield) in
            textfield.placeholder = placeholder
        }
        
        let action = UIAlertAction(title: "commonOk".WA_localized,
                                   style: .default) { [unowned alertController] _ in
            if let text = alertController.textFields?.first?.text {
                handler(text)
            }
        }
        
        alertController.addAction(action)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @objc func WA_showAlert(title: String?, message: String?, handler: ((UIAlertAction) -> Void)? = nil) {
        let okAction = UIAlertAction(title: "commonOk".WA_localized,
                                     style: .default,
                                     handler: handler)

        WA_showAlert(title: title, message: message, actions: [okAction])
    }
    
    @objc func WA_showAlert(title: String?, message: String?, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        actions.forEach({alertController.addAction($0)})
        alertController.view.accessibilityViewIsModal = true
        present(alertController, animated: true, completion: nil)
    }
}

