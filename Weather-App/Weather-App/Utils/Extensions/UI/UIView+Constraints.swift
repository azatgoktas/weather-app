//
//  UIView+Constraints.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

extension UIView {

    /// Docks view with given parameters
    ///
    /// - Parameters:
    ///   - view: Target view
    ///   - constants: Edge margins
    ///   - excludedEdge: Edge to exclude
    @objc func WA_dock(_ view: UIView,
                       insets: UIEdgeInsets = .zero,
                       excludingEdge excludedEdge: UIRectEdge = []) {
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        if !excludedEdge.contains(.bottom) {
            view.bottomAnchor.constraint(equalTo: bottomAnchor,
                                         constant: insets.bottom).isActive = true
        }

        if !excludedEdge.contains(.top) {
            view.topAnchor.constraint(equalTo: topAnchor,
                                      constant: insets.top).isActive = true
        }

        if !excludedEdge.contains(.left) {
            view.leadingAnchor.constraint(equalTo: leadingAnchor,
                                          constant: insets.left).isActive = true
        }

        if !excludedEdge.contains(.right) {
            view.trailingAnchor.constraint(equalTo: trailingAnchor,
                                           constant: insets.right).isActive = true
        }
    }
    
    /// Adds a view to view with insets
    ///
    /// - Parameters:
    ///   - view: The view to be added
    ///   - insets: Insets to apply
    func WA_addSubview(_ view: UIView, insets: UIEdgeInsets? = nil, excludingEdge excludedEdge: UIRectEdge = []) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        if let insets = insets {
            WA_dock(view, insets: insets, excludingEdge: excludedEdge)
        }
    }
}
