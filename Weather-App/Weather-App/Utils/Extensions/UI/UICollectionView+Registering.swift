//
//  UICollectionView+Registering.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

extension UICollectionView {

    func WA_registerNibReusableCell<T: UICollectionViewCell>(_ type: T.Type) {
        let description = String(describing: type)
        register(type.defaultNib, forCellWithReuseIdentifier: description)
    }

    func WA_dequeueReusableCell<T: UICollectionViewCell>(_ indexPath: IndexPath, type: T.Type = T.self) -> T {
        let description = String(describing: type)
        if let cell = dequeueReusableCell(withReuseIdentifier: description, for: indexPath) as? T {
            return cell
        }
        fatalError("Failed to dequeue a cell with identifier \(description) matching type \(type.self). "
            + "Check that the reuseIdentifier is set properly in your XIB/Storyboard "
            + "and that you registered the cell beforehand")
    }

    func WA_registerNibSupplementaryView<T: UICollectionReusableView>(_ type: T.Type, kind: String) {
        let description = String(describing: type)
        register(type.defaultNib, forSupplementaryViewOfKind: kind, withReuseIdentifier: description)
    }

    func WA_dequeueSupplementaryView<T: UICollectionReusableView>(_ indexPath: IndexPath,
                                                                  type: T.Type = T.self,
                                                                  kind: String) -> T {

        let description = String(describing: type)
        if let supplementaryView = dequeueReusableSupplementaryView(ofKind: kind,
                                                                    withReuseIdentifier: description,
                                                                    for: indexPath) as? T {
            return supplementaryView
        }
        fatalError("Failed to dequeue a supplementary view with identifier \(description)"
            + "matching type \(type.self).")
    }
}
