//
//  WeatherHeaderView+Styling.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

extension WeatherHeaderView {
    
    fileprivate enum Constant {
        static let titleLabelFontSize: CGFloat = 18.0
    }
    
    func WA_applyStyling() {
        containerView.backgroundColor = .white
        titleLabel.font = .systemFont(ofSize: Constant.titleLabelFontSize, weight: .semibold)
    }
}
