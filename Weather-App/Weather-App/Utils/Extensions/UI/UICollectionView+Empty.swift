//
//  UICollectionView+Empty.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

extension UICollectionView {
    
    struct Holder {
        static let emptyView = EmptyView()
    }
    
    func WA_setEmptyView(presentation: EmptyPresentation) {
        Holder.emptyView.fill(presentation)
    }
    
    func WA_setEmptyView(hidden: Bool) {
        backgroundView = hidden ? nil : Holder.emptyView
    }
}
