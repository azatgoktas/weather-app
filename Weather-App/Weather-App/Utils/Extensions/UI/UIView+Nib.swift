//
//  UIView+Nib.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

extension UIView {

    /// Loads nib and docks to view
    func ownFirstNib() {
        guard let loadedViews = Bundle.main.loadNibNamed(String(describing: classForCoder), owner: self) else {
            return
        }

        guard let loadedView = loadedViews.first as? UIView else {
            return
        }

        addSubview(loadedView)
        loadedView.translatesAutoresizingMaskIntoConstraints = false
        WA_dock(loadedView)
    }
}
