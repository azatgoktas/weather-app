//
//  WeatherHeaderView.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

final class WeatherHeaderView: UICollectionReusableView {
    @IBOutlet private(set) weak var containerView: UIView!
    @IBOutlet private(set) weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        WA_applyStyling()
    }
    
}

// MARK: Public

extension WeatherHeaderView {
    func fill(_ presentation: WeatherListPresentation) {
        titleLabel.text = presentation.title
    }
}
