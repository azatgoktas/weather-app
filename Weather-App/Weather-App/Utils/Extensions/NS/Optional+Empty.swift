//
//  Optional+Empty.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation

extension Swift.Optional where Wrapped: EmptyValueRepresentable {

    var orEmpty: Wrapped {
        switch self {
        case .some(let value):
            return value
        case .none:
            return Wrapped.emptyValue
        }
    }
}
