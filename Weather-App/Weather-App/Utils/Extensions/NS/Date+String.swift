//
//  Date+String.swift
//  Weather-App
//
//  Created by Azat Goktas on 26.01.2021.
//

import Foundation

extension Date {
    var day: String {
        let dateFormatter = DateFormatter()
        return dateFormatter.weekdaySymbols[Calendar.current.component(.weekday, from: self) - 1]
    }
}
