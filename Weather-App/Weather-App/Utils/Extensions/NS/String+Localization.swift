//
//  String+Localization.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import Foundation

extension String {
    var WA_localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
