//
//  AppRouter.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

final class AppRouter {
    var window: UIWindow

    init(window: UIWindow) {
        self.window = window
    }

    /// Application starts here. Start application routing here.
    func start() {
        let weatherViewController = WeatherBuilder.make()
        let navigationController = BaseNavigationController(rootViewController: weatherViewController)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}
