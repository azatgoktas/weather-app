//
//  WeatherPresentation.swift
//  Weather-App
//
//  Created by Azat Goktas on 26.01.2021.
//

import Foundation

struct WeatherListPresentation {
    let title: String
    let items: [WeatherListItemPresentation]
}

extension WeatherListPresentation: Equatable {}

struct WeatherListItemPresentation {
    let imageUrl: String
    let description: String
    let temprature: String
    let time: String
}

extension WeatherListItemPresentation: Equatable {}
