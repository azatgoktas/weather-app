//
//  WeatherViewController.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import UIKit

final class WeatherViewController: BaseViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var segmentedControl: UISegmentedControl!
    
    private var presentation: [WeatherListPresentation] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var viewModel: WeatherViewModelProtocol! {
        didSet {
            viewModel.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.load()
        setupView()
    }
    
    @IBAction func segmentedControllerDidChange(_ sender: UISegmentedControl) {
        viewModel.changeData(index: sender.selectedSegmentIndex)
    }
    
}

// MARK: - Private

private extension WeatherViewController {
    
    func setupView() {
        
        collectionView.WA_registerNibReusableCell(WeatherItemCollectionViewCell.self)
        collectionView.WA_registerNibSupplementaryView(WeatherHeaderView.self,
                                                       kind: UICollectionView.elementKindSectionHeader)
        
        collectionView.collectionViewLayout = generateLayout()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let selectCityButton = UIBarButtonItem(barButtonSystemItem: .add,
                                               target: self,
                                               action: #selector(selectCityButtonDidTap))
        navigationItem.rightBarButtonItem = selectCityButton
        
        segmentedControl.setTitle("live".WA_localized, forSegmentAt: 0)
        segmentedControl.setTitle("local".WA_localized, forSegmentAt: 1)
        
    }
    
    @objc func selectCityButtonDidTap() {

        WA_showAlert(title: nil,
                     message: "type-city".WA_localized,
                     placeholder: "city".WA_localized) { [weak self] (city) in
            self?.viewModel.getWeather(query: city)
        }
    }
    
    func generateSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .fractionalWidth(1.0)
        )
        
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .absolute(140),
            heightDimension: .absolute(186)
        )
        
        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: groupSize,
            subitem: item,
            count: 1
        )
        
        group.contentInsets = NSDirectionalEdgeInsets(
            top: 5,
            leading: 5,
            bottom: 5,
            trailing: 5
        )
        
        let headerSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(44)
        )
        
        let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
            layoutSize: headerSize,
            elementKind: UICollectionView.elementKindSectionHeader,
            alignment: .top
        )
        
        let section = NSCollectionLayoutSection(group: group)
        section.boundarySupplementaryItems = [sectionHeader]
        section.orthogonalScrollingBehavior = .continuous
        
        return section
    }
    
    func generateLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionIndex: Int,
                                                            layoutEnvironment: NSCollectionLayoutEnvironment)
            -> NSCollectionLayoutSection? in
            return self.generateSection()
        }
        return layout
    }
}

// MARK: - WeatherViewModelDelegate

extension WeatherViewController: WeatherViewModelDelegate {
    
    func handleViewModelOutput(_ output: WeatherViewModelOutput) {
        DispatchQueue.main.async {
            switch output {
            case .updateTitle(let title):
                self.title = title
            case .setEmptyView(let presentation):
                self.collectionView.WA_setEmptyView(presentation: presentation)
            case .showAlert(message: let message):
                self.WA_showAlert(title: "commonError".WA_localized, message: message)
            case .presentations(let presentations):
                self.presentation = presentations
            case .setEmptyViewHidden(let hidden):
                self.collectionView.WA_setEmptyView(hidden: hidden)
            case .setSegmentedController(let index):
                self.segmentedControl.selectedSegmentIndex = index
            }
        }
    }
}

// MARK: - Collection View Data Source

extension WeatherViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presentation.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presentation[section].items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.WA_dequeueReusableCell(indexPath, type: WeatherItemCollectionViewCell.self)
        cell.fill(presentation[indexPath.section].items[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.WA_dequeueSupplementaryView(indexPath,
                                                                    type: WeatherHeaderView.self,
                                                                    kind: kind)
        headerView.fill(presentation[indexPath.section])
        return headerView
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension WeatherViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
}
