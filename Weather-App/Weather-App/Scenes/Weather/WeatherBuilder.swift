//
//  WeatherBuilder.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import Foundation

final class WeatherBuilder {
    static func make() -> WeatherViewController {
        let viewController = WeatherViewController()
        let service = WeatherService()
        let viewModel = WeatherViewModel(service: service)
        viewController.viewModel = viewModel
        return viewController
    }
}
