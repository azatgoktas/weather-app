//
//  WeatherViewModel.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import Foundation

final class WeatherViewModel: WeatherViewModelProtocol {
    
    var delegate: WeatherViewModelDelegate?
    private let service: WeatherServiceProtocol
    private var city: String?
    
    private enum DataType: Int {
        case api = 0
        case local = 1
    }
    
    init(service: WeatherServiceProtocol) {
        self.service = service
    }
    
    func load() {
        setTitle()
        setEmptyView()
        setEmptyView(hidden: false)
    }
    
    func getWeather(query: String) {
        self.city = query
        service.getWeather(query: query) { [weak self] (result) in
            switch result {
            case .failure(let error):
                self?.delegate?.handleViewModelOutput(.showAlert(message: error.localizedDescription))
            case .success(let response):
                self?.mapToPresentation(response: response)
            }
            self?.delegate?.handleViewModelOutput(.setSegmentedController(DataType.api.rawValue))
        }
    }
    
    func changeData(index: Int) {
        let type = DataType(rawValue: index)
        
        switch type {
        case .api:
            if let city = city {
                getWeather(query: city)
            } else {
                delegate?.handleViewModelOutput(.presentations([]))
                setEmptyView(hidden: false)
            }
        case .local:
            getLocalData()
        default:
            break
        }
    }
}


// MARK: - Private

private extension WeatherViewModel {
    func setTitle() {
        delegate?.handleViewModelOutput(.updateTitle(("weatherTitle".WA_localized)))
    }
    
    func setEmptyView() {
        let presentation = EmptyPresentation(message: "select-city".WA_localized,
                                             imageName: "cities")
        delegate?.handleViewModelOutput(.setEmptyView(presentation))
    }
    
    func setEmptyView(hidden: Bool) {
        delegate?.handleViewModelOutput(.setEmptyViewHidden(hidden))
    }
    
    func mapToPresentation(response: WeatherResponse) {
        let list = response.list
        
        let groupedList = Dictionary(grouping: list) { (forecast) -> Date in
            return Calendar.current.startOfDay(for: forecast.date)
        }.sorted(by: { $0.key < $1.key })
        
        let presentations: [WeatherListPresentation] = groupedList.map { (key, value) in
            let weatherList = value.map {
                WeatherListItemPresentation(imageUrl: ($0.weather.first?.iconUrl).orEmpty,
                                            description: ($0.weather.first?.main).orEmpty,
                                            temprature: $0.main.readebleTemprature,
                                            time: $0.readableTime)
            }
            
            return WeatherListPresentation(title: key.day, items: weatherList)
        }
        
        setEmptyView(hidden: !presentations.isEmpty)
        delegate?.handleViewModelOutput(.presentations(presentations))
    }
    
    func getLocalData() {
        if let path = Bundle.main.path(forResource: "WeatherResponse", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .secondsSince1970
                let response = try decoder.decode(WeatherResponse.self, from: data)
                mapToPresentation(response: response)
            } catch {
                delegate?.handleViewModelOutput(.showAlert(message: error.localizedDescription))
            }
        }
    }
}
