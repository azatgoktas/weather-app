//
//  WeatherContracts.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import Foundation

protocol WeatherViewModelProtocol {
    var delegate: WeatherViewModelDelegate? { get set }
    func load()
    func getWeather(query: String)
    func changeData(index: Int)
}

enum WeatherViewModelOutput: Equatable {
    case updateTitle(String)
    case setEmptyView(EmptyPresentation)
    case setEmptyViewHidden(Bool)
    case showAlert(message: String)
    case presentations([WeatherListPresentation])
    case setSegmentedController(Int)
}

enum WeatherViewRoute {
    case detail
}

protocol WeatherViewModelDelegate: class {
    func handleViewModelOutput(_ output: WeatherViewModelOutput)
}
