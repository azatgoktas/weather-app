//
//  WeatherRequest.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation

struct TimelineRequest: Request {
    var method: HTTPMethod = .get
    var path: String = "forecast"
    var task: HTTPTask
    
    let query: String
    let appid: String
    
    init (query: String, appid: String) {
        self.query = query
        self.appid = appid
        
        task = .requestParameters(bodyParameters: nil,
                                  urlParameters: ["q": query,
                                                  "appid": appid])
    }
}
