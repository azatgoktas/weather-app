//
//  GetOperatable.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import Foundation

struct GetOperatable: NetworkOperatable {
  var methodType: MethodType { return .get }
  
  var body: Encodable
  var urlString: String
  var urlRequest: URLRequest { return createRequest() }
}

private extension GetOperatable {
  
  func createRequest() -> URLRequest {
    var urlStr = urlString
    if let dict = body.dictionary {
      urlStr += "?" + dict.map({"\($0)=\($1)"}).joined(separator: "&")
    }
    var request = URLRequest(url: URL(string: urlStr)!, cachePolicy: cachePolicy, timeoutInterval: timeOut)
    request.allHTTPHeaderFields = headers
    request.httpMethod = methodType.rawValue
    
    return request
  }
}

extension Data {
  func decode<T: Decodable>() throws -> T {
    return try JSONDecoder().decode(T.self, from: self)
  }
}

extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
  
  var data: Data? {
    return try? JSONEncoder().encode(self)
  }
}
