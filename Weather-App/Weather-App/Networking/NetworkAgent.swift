//
//  NetworkAgent.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import Foundation

class Agent {
  func execute<T: NetworkOperatable, V: Decodable>(operatable: T, then: @escaping (Result<V,Error>) -> Void) {
    URLSession.shared.dataTask(with: operatable.urlRequest) { (data, response, error) in
      if let error = error {
        then(.failure(error))
        return
      }
      
      guard let data = data,
        let object = try? JSONDecoder().decode(V.self, from: data) else {
          //TODO: Default error
//          then(.failure())
          return
      }
      
      then(.success(object))
    }
  }
}
