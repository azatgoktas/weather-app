//
//  WeatherResponse.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation


struct City : Codable {
    let name: String
    let country: String
}

struct Main : Codable {
    let temp : Double
    let temp_min : Double
    let temp_max : Double
    
    /// Returns readable temprature for your locale
    var readebleTemprature: String {
        let formatter = MeasurementFormatter()
        let measurement = Measurement(value: temp, unit: UnitTemperature.kelvin)
        return formatter.string(from: measurement)
    }
}

struct Weather : Codable {
    let main : String
    let description : String
    let icon: String
    
    var iconUrl: String {
        return "http://openweathermap.org/img/wn/\(icon)@2x.png"
    }
}

/// Forecast structure
struct Forecast : Codable {
    let date : Date
    let main : Main
    let weather : [Weather]

    enum CodingKeys : String, CodingKey {
        case date = "dt"
        case main
        case weather
    }
    
    var readableTime: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        return dateFormatter.string(from: date)
    }
}

struct WeatherResponse : Codable {
    let message : Int
    let list : [Forecast]
    let city : City
}
