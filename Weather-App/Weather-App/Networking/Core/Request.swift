//
//  Request.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation

protocol Request {
    var method: HTTPMethod { get }
    var path: String { get }
    var task: HTTPTask { get }
}

extension Request {

    /// Responsible from configuring request
    ///
    /// - Parameter request: A request object that conforms Request Protocol
    /// - Returns: Returns a URLRequest Object
    /// - Throws: Throwing error if configuring request fails
    func buildRequest(from request: Request) throws -> URLRequest {
        let baseUrl = Environment.environmentBaseUrl
        var urlRequest = URLRequest(url: baseUrl.appendingPathComponent(request.path))
        self.addDefaultHeaders(request: &urlRequest)
        urlRequest.httpMethod = request.method.rawValue
        do {
            switch request.task {
            case .request:
                break
            case .requestParameters(let bodyParameters, let urlParameters):

                try self.configureParamaters(bodyParameters: bodyParameters,
                                             urlParameters: urlParameters,
                                             request: &urlRequest)

            case .requestParametersAndHeaders(let bodyParameters, let urlParameters, let headers):
                self.addAdditionalHeaders(headers: headers, request: &urlRequest)
                try self.configureParamaters(bodyParameters: bodyParameters,
                                             urlParameters: urlParameters,
                                             request: &urlRequest)
            }

            return urlRequest
        } catch {
            throw error
        }

    }

    func configureParamaters(bodyParameters: Parameters?,
                             urlParameters: Parameters?,
                             request: inout URLRequest) throws {
        do {
            if let bodyParameters = bodyParameters {
                request.httpBody = try JSONSerialization.data(withJSONObject: bodyParameters, options: [.prettyPrinted])
            }

            if let urlParameters = urlParameters {
                guard let url = request.url?.absoluteString else {
                    return
                }
                
                request.url = getUrl(with: url, urlParameters: urlParameters)
            }
        } catch {
            throw error
        }
    }
    
    /// Creates a URL with the given base URL.
    /// - Parameter baseURL: The base URL string.
    /// - Parameter urlParamaters: The url paramaters
    /// - Returns: An optional `URL`.
    private func getUrl(with url: String, urlParameters: Parameters) -> URL? {
        // Create a URLComponents instance to compose the url.
        guard var urlComponents = URLComponents(string: url) else {
            return nil
        }
        
        
        let queryItems = urlParameters.map { (key: String, value: Any) -> URLQueryItem in
            let valueString = String(describing: value)
            return URLQueryItem(name: key, value: valueString)
        }
        
        // Add query items to the request URL
        urlComponents.queryItems = queryItems
        
        return urlComponents.url
    }

    /// Add additional headers to specific request
    ///
    /// - Parameters:
    ///   - headers: HTTPHeaders
    ///   - request: Request
    func addAdditionalHeaders(headers: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = headers else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }

    /// Adding Default Headers To All Requests
    ///
    /// - Parameter request: Request
    func addDefaultHeaders(request: inout URLRequest) {
        let headers: HTTPHeaders = [:]
        self.addAdditionalHeaders(headers: headers, request: &request)
    }
}

extension Request {
    var header: HTTPHeaders {
        return [:]
    }
}
