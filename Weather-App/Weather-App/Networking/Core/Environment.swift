//
//  Environment.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation

struct Environment {

    enum NetworkEnvironment {
        case production
        case staging
    }

    static let environment = NetworkEnvironment.production

    static var environmentBaseUrl: URL {
        switch environment {
        case .production: return URL(string: "http://api.openweathermap.org/data/2.5")!
        case .staging: return URL(string: "somebaseurl")! 
        }
    }

}
