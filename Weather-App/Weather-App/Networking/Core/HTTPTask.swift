//
//  HTTPTask.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation

typealias HTTPHeaders = [String: String]
typealias Parameters = [String: Any]

/// HTTP Request Tasks
///
/// - request: Simple Request without any parameters
/// - requestParametersbodyParameters:Parameters?: Body parameters
/// - : Request with body parameters
/// - requestParametersAndHeadersbodyParameters:Parameters?: Body Parameters And Custom Headers
/// - : Request Body Parameteres and Custom Headers
enum HTTPTask {
    
    case request

    case requestParameters(bodyParameters: Parameters?,
        urlParameters: Parameters?)

    case requestParametersAndHeaders(bodyParameters: Parameters?,
        urlParameters: Parameters?,
        additionHeaders: HTTPHeaders?)
}
