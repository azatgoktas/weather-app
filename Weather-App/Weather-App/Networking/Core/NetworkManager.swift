//
//  NetworkManager.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation

struct NetworkManager {

    let dispatcher = NetworkDispatcher()

    func execute<T: Decodable>(request: Request, _ completion: @escaping (Result<T, Error>) -> Void) {
        dispatcher.request(request) { (data, urlResponse, _) in
            if let response = urlResponse as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                guard let responseData = data else {return}

                switch result {
                case .success:
                    do {
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .secondsSince1970
                        let apiResponse = try decoder.decode(T.self, from: responseData)
                        completion(.success(apiResponse))
                    } catch(let err) {
                        completion(.failure(err))
                    }
                case .failure:
                    do {
                        let decoder = JSONDecoder()
                        let apiResponse = try decoder.decode(FailureModel.self, from: responseData)
                        completion(.failure(apiResponse))
                    } catch(let err) {
                        completion(.failure(err))
                    }
                }
            }
        }
    }

    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<Any, Error> {
        switch response.statusCode {
        case 200...299: return .success(response)
        case 401...500: return .failure(NetworkResponses.authenticationError)
        case 501...599: return .failure(NetworkResponses.badRequest)
        case 600: return .failure(NetworkResponses.outdated)
        default: return .failure(NetworkResponses.failed)
        }
    }
}
