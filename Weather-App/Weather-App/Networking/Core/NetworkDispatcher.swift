//
//  NetworkDispatcher.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation

public typealias NetworkDispatcherCompletion = (_ data: Data?, _ response: URLResponse?, _ error: Error? ) -> Void

protocol NetworkDispatcherProtocol {
    func request(_ request: Request, completion: @escaping NetworkDispatcherCompletion)
}

final class NetworkDispatcher: NetworkDispatcherProtocol {

    private let defaultSession = URLSession(configuration: .default)

    init() {
    
    }

    /// Responsible from making requests
    ///
    /// - Parameters:
    ///   - request: A request object that conforms Request Protocol
    ///   - completion: Completion block to get data, response, error
    func request(_ request: Request, completion: @escaping NetworkDispatcherCompletion) {
        do {
            let urlRequest = try request.buildRequest(from: request)
            
            let dataTask = defaultSession.dataTask(with: urlRequest) { (data, response, error) in
                completion(data, response, error)
            }
            
            dataTask.resume()
            
        } catch let error {
            print(error)
            return
        }
    
    }
}
