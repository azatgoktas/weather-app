//
//  Response.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation


enum NetworkResponses: String, Error {
    case success
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

struct NetworkResponse<T> {
    var request: URLRequest?
    var response: HTTPURLResponse?
    var data: Data?
    var result: Result<T, Error>
    var object: T?
    var jsonData: Any?
}
