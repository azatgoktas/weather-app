//
//  FailureModel.swift
//  Weather-App
//
//  Created by Azat Goktas on 26.01.2021.
//

import Foundation

struct FailureModel: Decodable {
    let cod: String
    let message: String
}

extension FailureModel: LocalizedError {
    var failureReason: String? {
        return message
    }
}
