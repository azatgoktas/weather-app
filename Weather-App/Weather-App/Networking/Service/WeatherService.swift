//
//  WeatherService.swift
//  Weather-App
//
//  Created by Azat Goktas on 25.01.2021.
//

import Foundation

protocol WeatherServiceProtocol {
    func getWeather(query: String, completion:  @escaping (Result<WeatherResponse, Error>) -> Void)
}

final class WeatherService: WeatherServiceProtocol {
    
    fileprivate enum Constant {
        static let appId = "ff394691299d9c652848afb7a05f5b3d"
    }

    let manager = NetworkManager()

    func getWeather(query: String, completion:  @escaping (Result<WeatherResponse, Error>) -> Void) {
        manager.execute(request: TimelineRequest(query: query,
                                                 appid: Constant.appId)) { (result: Result<WeatherResponse, Error>) in
            completion(result)
        }
    }
}
