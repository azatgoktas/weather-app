//
//  PostOperatable.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import Foundation

struct PostOperatable: NetworkOperatable {
  var methodType: MethodType { return .post }
  
  var body: Encodable
  var urlString: String
  var urlRequest: URLRequest { return createRequest() }
}

private extension PostOperatable {
  
  func createRequest() -> URLRequest {
    var request = URLRequest(url: URL(string: urlString)!, cachePolicy: cachePolicy, timeoutInterval: timeOut)
    request.httpBody = body.data
    request.allHTTPHeaderFields = headers
    request.httpMethod = methodType.rawValue
    
    return request
  }
}
