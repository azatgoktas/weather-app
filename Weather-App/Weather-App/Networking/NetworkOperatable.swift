//
//  NetworkOperatable.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import Foundation

protocol NetworkOperatable {
  var methodType: MethodType { get }
  var contentType: ContentType { get set }
  var timeOut: TimeInterval { get set }
  var cachePolicy: URLRequest.CachePolicy {get}
  var headers: [String: String] { get set }
  
  var urlRequest: URLRequest { get }
}

extension NetworkOperatable {
  var cachePolicy: URLRequest.CachePolicy {return .useProtocolCachePolicy}
  var timeOut: TimeInterval { get { return 60 } set { } }
  var contentType: ContentType { get { return .json } set { } }
  var headers: [String: String] { get { return [:] } set { } }
}
