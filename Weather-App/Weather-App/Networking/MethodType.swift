//
//  MethodType.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import Foundation

enum MethodType: String {
  case get = "GET"
  case post = "POST"
}
