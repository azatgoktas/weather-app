//
//  ContentType.swift
//  Weather-App
//
//  Created by Azat Goktas on 24.01.2021.
//

import Foundation

public enum ContentType:String{
    case json = "application/json; charset=utf-8"
    case urlencoded = "application/x-www-form-urlencoded"
    case none = ""
}
